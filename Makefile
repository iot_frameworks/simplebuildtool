# Makefile for building the sample on Linux, with protoc and the gRPC plugins
# installed on the system following the instructions in gRPC's INSTALL file.
#
# Parts of are taken verbatim from examples/cpp/helloworld/Makefile, so if you
# want to be strict about these things - gRPC's (3-clause BSD) license applies
# to these parts.
# 
# Eli Bendersky [http://eli.thegreenplace.net]
# This code is in the public domain.
HOST_SYSTEM = $(shell uname | cut -f 1 -d_)
SYSTEM ?= $(HOST_SYSTEM)
CXX = g++
CPPFLAGS += -I/usr/local/include -pthread
CXXFLAGS += -std=c++11
LDFLAGS += -L/usr/local/lib -lgrpc++_unsecure -lgrpc -lprotobuf -lpthread -ldl

PROTOC = protoc
GRPC_CPP_PLUGIN = grpc_cpp_plugin
GRPC_CPP_PLUGIN_PATH ?= `which $(GRPC_CPP_PLUGIN)`
GRPC_PYTHON_PLUGIN = grpc_python_plugin
GRPC_PYTHON_PLUGIN_PATH ?= `which $(GRPC_PYTHON_PLUGIN)`
GRPC_NODE_PLUGIN = 	grpc_node_plugin
GRPC_NODE_PLUGIN_PATH ?= `which $(GRPC_NODE_PLUGIN)`

PROTOS_PATH = ./IotivityLogger/src
vpath %.proto $(PROTOS_PATH)
EXECUTABLES = iot_server

all: $(EXECUTABLES) iot_pb.js

.PRECIOUS: %.grpc.pb.cc
%.grpc.pb.cc: %.proto
	$(PROTOC) -I $(PROTOS_PATH) --grpc_out=. --plugin=protoc-gen-grpc=$(GRPC_CPP_PLUGIN_PATH) $<

.PRECIOUS: %.pb.cc
%.pb.cc: %.proto
	$(PROTOC) -I $(PROTOS_PATH) --cpp_out=. $<

iot_server: iot.pb.o iot.grpc.pb.o iot_server.o
	$(CXX) $^ $(LDFLAGS) -o $@

# Rule for producing the Python protobuf bindings
iot_pb.js: iot.proto
	$(PROTOC) -I $(PROTOS_PATH) --js_out=import_style=commonjs,binary:. \
		--plugin=protoc-gen-grpc=$(GRPC_NODE_PLUGIN_PATH) $<

clean:
	rm -f $(EXECUTABLES) *.pb.cc *.pb.h *.pb.o *.o *_pb.js 
	
PROTOC_CMD = which $(PROTOC)
PROTOC_CHECK_CMD = $(PROTOC) --version | grep -q libprotoc.3
PLUGIN_CHECK_CMD = which $(GRPC_CPP_PLUGIN)
HAS_PROTOC = $(shell $(PROTOC_CMD) > /dev/null && echo true || echo false)
ifeq ($(HAS_PROTOC),true)
HAS_VALID_PROTOC = $(shell $(PROTOC_CHECK_CMD) 2> /dev/null && echo true || echo false)
endif
HAS_PLUGIN = $(shell $(PLUGIN_CHECK_CMD) > /dev/null && echo true || echo false)

SYSTEM_OK = false
ifeq ($(HAS_VALID_PROTOC),true)
ifeq ($(HAS_PLUGIN),true)
SYSTEM_OK = true
endif
endif


