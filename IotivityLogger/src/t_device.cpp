/*
 * t_device.cpp
 *
 *  Created on: 19-Jun-2017
 *      Author: jotirling
 */
#include "headers.hpp"

using namespace std;

int DisFlag=0;

extern std::vector<std::string>t_thingsvector;

t_device::t_device() {
	Coluri = "";
	pro_data.head = NULL;
}

void t_device::t_device_display()
{
	cout << "Coluri :" <<this->Coluri  <<endl;

      for (auto it =this->resMap.begin();it!=this->resMap.end();it++)

	cout << "map uri:" <<this->pro_data.head->map<<endl;
	cout << "Property Data: "<<endl;

	this->pro_data.Display();

}

void t_device::setDeviceCallback(devUpdateCall devCallback)
{
  this->devCallback=devCallback;
}


void t_device::setresourcecallback()
{

    for (auto it =this->resMap.begin();it!=this->resMap.end();it++)
    {
	it->second->setnotifycallback(std::bind( &t_device::notify, this, _1,_2,_3,_4));
    }
}


void t_device::notify(OCRepPayloadValue* rep,string request,string uri, int flag)
{

    struct property *temp ;

	    uri.erase(0,47);

	while (rep)
	  {

	if(request == "discovery")
	  {

	    this->pro_data.createnode( );
	    this->pro_data.head->type = rep->type;
	    this->pro_data.head->name = rep->name;
	    this->pro_data.head->map =uri;

	  }

	temp=this->pro_data.head;

	while (temp)
	  {
	    if(uri == temp->map)
	      {

	 		switch ( temp->type )
			{

			  case 0:
				  cout << "OCREP_PROP_NULL_t_device " << endl;
				  break;

			  case 1:
				  cout << "OCREP_PROP_INT_t_device " << endl;
				  temp->i =rep->i;
				  break;

			  case 2:
				  cout << "OCREP_PROP_DOUBLE_t_device " << endl;
				  temp->d =rep->d;
				  break;

			  case 3:
				  cout << "OCREP_PROP_BOOL_t_device " << endl;
				  temp->b =rep->b;
			              break;

			      case 4:
				  cout << "OCREP_PROP_STRING_t_device " << endl;
				  temp->str =rep->str;
				      break;

			      case 5:
				  cout << "OCREP_PROP_NULL_t_device " << endl;
				      break;
			      case 6:
				  cout << "OCREP_PROP_NULL_t_device " << endl;
				      break;
			      case 7:
						{
						cout << "OCREP_PROP_ARRAY_" << endl;
						temp->intArray.clear();
						OCRepPayloadValueArray ArrayValue;
						ArrayValue = rep->arr;

						switch ( ArrayValue.type )
						{

							  case 0:
								  cout << "OCRepPayloadValueArray_NULL" << endl;

								  break;

							  case 1:{
								  cout << "OCRepPayloadValueArray_iArray" << endl;

									for(int i = 0; i< MAX_REP_ARRAY_DEPTH ; i++){
										cout<< "[" << ArrayValue.iArray[i] << "]\n";
										temp->intArray.push_back(ArrayValue.iArray[i]);
									}

							  		break;
							  }
						}

						break;
						}

					  default:
					  cout<<"in default"<<endl;
			 }
	      }

	    temp=temp->next;
	  }
        rep = rep->next;

      }

		cout << "PRINTING THE SIZE t_thingsvector.size():"<< t_thingsvector.size() << endl;




       if( (request == "get") )
	{

		if(( this->Coluri=="5b000000-ed00-4000-6301-000011010000/SMWS1M_01") && (flag == (this->resMap.size())-1))
		{
    	  cout << "sending the get response to GRPC Server ................."<< endl;

    	  		client grpcClient(
    	  				grpc::CreateChannel(GRPCNodeJsClient,
    	  						grpc::InsecureChannelCredentials()));
    	  		grpcClient.NondiscoveryResponse(this);
		}

		else if((flag == this->resMap.size()))
		{
    	  cout << "sending the get response to GRPC Server ................."<< endl;

    	  		client grpcClient(
    	  				grpc::CreateChannel(GRPCNodeJsClient,
    	  						grpc::InsecureChannelCredentials()));
    	  		grpcClient.NondiscoveryResponse(this);
		}

}
      else if(request == "post")
	{
	cout << "sending the post response to GRPC Server ................."<< endl;

		client grpcClient(
				grpc::CreateChannel(GRPCNodeJsClient,
						grpc::InsecureChannelCredentials()));
		grpcClient.NondiscoveryResponse(this);

	}




	if( (request == "observe") )
{

if(( this->Coluri=="5b000000-ed00-4000-6301-000011010000/SMWS1M_01") && (flag == (this->resMap.size())-1))
{
	 cout << "sending the observe response to GRPC Server ................."<< endl;

			 client grpcClient(
					 grpc::CreateChannel(GRPCNodeJsClient,
							 grpc::InsecureChannelCredentials()));
			 grpcClient.NondiscoveryResponse(this);
}

else if((flag == this->resMap.size()))
{
	 cout << "sending the observe response to GRPC Server ................."<< endl;

			 client grpcClient(
					 grpc::CreateChannel(GRPCNodeJsClient,
							 grpc::InsecureChannelCredentials()));
			 grpcClient.NondiscoveryResponse(this);
}

}

      else if( (request == "discovery") )
	{
	  DisFlag++;
	  this->devCallback();
	}

}
