#include "headers.hpp"

void Auto_Discovery(int);
void missing();
void DeleteThings();
using namespace OC;
using namespace std;
#include <cstring>

std::map<string, t_resource*> t_resourcemap;
std::map<string, t_resource*>::iterator t_resourcemapItr;

std::map<string, t_resource*> t_resourceColmap;
std::map<string, t_resource*>::iterator t_resourceColmapItr;

std::map<string, t_things*> t_thingsmap;
std::map<string, t_things*> ::iterator t_thingsmapItr;

std::vector <std::string> missingVector ;
std::vector <std::string> ::iterator missingVectorItr;

static OCConnectivityType connectivityType = CT_ADAPTER_IP;

std::map<std::string, int> RepPayloadMap;

bool auto_Discovery = false;
void SecondDiscovery(string uuid);
extern string reqType ;

std::vector<std::string>t_thingsvector;

std::map<std::string,std::string>missDiscoveryMap;
std::vector<std::string>missDiscovery;
std::vector<std::string>missPlatform;
std::vector<std::string>::iterator missPlatformit;
std::vector<std::string>::iterator uuidItr;


void foundResource(std::shared_ptr<OCResource> resource) {

	auto it = t_thingsmap.find(resource->sid());
	if (it != t_thingsmap.end()) {
	t_resource *temp = new t_resource;
	
	temp->resource = resource;
	temp->hostadd = resource->host();
	temp->resourceURI = resource->uri();
	temp->serverID = resource->sid();
	temp->key = temp->serverID + temp->resourceURI;

	if (!temp->resourceURI.compare("/oic/sec/doxm") == 0
			&& !temp->resourceURI.compare("/oic/sec/pstat") == 0
			&& !temp->resourceURI.compare("/oic/d") == 0
			&& !temp->resourceURI.compare("/oic/p") == 0
			&& !temp->resourceURI.compare("oic/d") == 0
			&& !temp->resourceURI.compare("oic/p") == 0
			&& !temp->resourceURI.compare("/a/light") == 0) {

cout << "\nRESOURCE_KEY :" << temp->key << endl;
cout << "RESOURCE_KEY :" << resource->host() << endl << endl;
const char* mm = temp->resourceURI.c_str();
setlogging1(OC_LOG_DEBUG,"OC_LOG_DEBUG","Discovered resource URI : %s", mm);


//setlogging1(OC_LOG_ALL,"OC_LOG_ALL","Hello, World!");
//setlogging1(OC_LOG_DEBUG,"OC_LOG_DEBUG","Hello, World!");
//setlogging1(OC_LOG_ERROR,"OC_LOG_ERROR","Hello, World!");
//setlogging1(OC_LOG_FATAL,"OC_LOG_FATAL","Hello, World!");
//setlogging1(OC_LOG_INFO,"OC_LOG_INFO","Hello, World!");

	missDiscovery.push_back(temp->serverID );
	missDiscoveryMap[temp->serverID] = temp->key;

		std::vector<std::string> interfaceVec;
		for (auto &resourceInterfaces : temp->resource->getResourceInterfaces()) {
//			setlogging1(OC_LOG_DEBUG,"OC_LOG_DEBUG","Discovered resource interface : ",resourceInterfaces);
			interfaceVec.push_back(resourceInterfaces);
		}

		if (std::find(interfaceVec.begin(), interfaceVec.end(), "oic.if.ll")
				!= interfaceVec.end()) {

			t_resourceColmap[temp->key] = temp;

		} else {

			t_resourcemap[temp->key] = temp;
		}

	}
}

	alarm(3);
}
void resource_discovery()
{
	std::ostringstream requestURI;

	cout << "In function Discovery" << endl;
	requestURI << OC_RSRVD_WELL_KNOWN_URI; // << "?rt=oic/res";
	OCPlatform::findResource("", requestURI.str(), CT_DEFAULT, &foundResource);

}

void receivedDeviceInfo(const OCRepresentation& rep)
{
string DevName;
rep.getValue("n", DevName);
if(DevName.size()!=NULL){

	t_things *temp = new t_things;

	rep.getValue("di", temp->uuid);
	rep.getValue("n", temp->thingsName);
	rep.getValue("dt", temp->thingsType);

	std::string thingsName = " thingsName " + temp->thingsName;
	std::string thingsType = " thingsType " + temp->thingsType;

//    setlogging1(OC_LOG_DEBUG,"OC_LOG_DEBUG","Discovered Device  : ",thingsName);
//	setlogging1(OC_LOG_DEBUG,"OC_LOG_DEBUG","Discovered Device  : ",temp->uuid);

	t_thingsmap[temp->uuid] = temp;

	}
}

void deviceInfo()
{
	cout << "\n\nIn function deviceInfo " << endl;
	std::string deviceDiscoveryURI = "/oic/d";
	OCPlatform::getDeviceInfo("", deviceDiscoveryURI, connectivityType,
			&receivedDeviceInfo);
}

void receivedPlatformInfo(const OCRepresentation& rep)
{
	std::string PlatId;
	rep.getValue("pi", PlatId);


	auto it = t_thingsmap.find(PlatId);
	if (it != t_thingsmap.end()) {
	    cout << "DEvice is there in Plarform Info" << endl;

if(PlatId.size()!=NULL){
	missPlatform.push_back(PlatId);

	auto it = t_thingsmap.find(PlatId);
	if (it != t_thingsmap.end()) {
		rep.getValue("mnmn", it->second->manufacturerName);
		rep.getValue("mnos", it->second->osName);
		rep.getValue("mnhw", it->second->hardwareVersion);
		rep.getValue("mnfv", it->second->firmwareVersion);

		std::string manufacturerName = " manufacturerName "
				+ it->second->manufacturerName;
		std::string osName = " osName " + it->second->osName;
		std::string hardwareVersion = " hardwareVersion "
				+ it->second->hardwareVersion;
		std::string firmwareVersion = " firmwareVersion "
				+ it->second->firmwareVersion;

//		setlogging1(OC_LOG_DEBUG,"OC_LOG_DEBUG","Discovered Platform : ",osName);
//		setlogging1(OC_LOG_DEBUG,"OC_LOG_DEBUG","Discovered Platform : ",PlatId);

		}
}
}
	else{

		for(auto it = t_resourcemap.cbegin(); it != t_resourcemap.cend(); ++it)
		{
		    std::cout <<"BEFORE_t_thingsmapPRINT=" <<it->first << endl;
	        if (!(it->first.find (PlatId)))
	    	t_resourcemap.erase(it->first);

		}
	}
}

void platformInfo()
{
	cout << "\n\nIn function platformInfo " << endl;
	std::string platformDiscoveryURI = "/oic/p";
	OCPlatform::getPlatformInfo("", platformDiscoveryURI, connectivityType,
			&receivedPlatformInfo);
}



void t_things_garbage()
{
	for (auto it = t_thingsmap.cbegin(); it != t_thingsmap.cend(); ++it) {
		if (!(it->second->manufacturerName.length())) {
		cout << "\n\nJUNK SERVER :" << it->first << endl;
			t_thingsmap.erase(it->first);
		}
	}
}

void Discovery_all()
{
//	t_resourcemap.clear();
//	t_resourceColmap.clear();
//	t_thingsmap.clear();

	missDiscovery.clear();
	missPlatform.clear();
	missDiscoveryMap.clear();

	cout << "In function Discovery_all " << endl;
	deviceInfo();
	sleep(6);
	platformInfo();
	sleep(6);

	resource_discovery();

	std::cout << " after Discovery_all " << '\n';
}


void t_things_device ()
{
        std::cout << " \nIN t_things_device : " << '\n';

        for (auto it_uid = t_thingsmap.cbegin (); it_uid != t_thingsmap.cend ();
             ++it_uid)
        {

                int i = 0;
                for (auto it_key = t_resourceColmap.cbegin ();
                     it_key != t_resourceColmap.cend (); ++it_key)
                {


                        if (!(it_key->first.find (it_uid->first)))
                        {
                                t_device *device = new t_device;


                                device->Coluri = it_key->first;
                                 for (auto it_key = t_resourcemap.cbegin ();
                                     it_key != t_resourcemap.cend (); ++it_key)
                                {


                                        if (!(it_key->first.find (device->Coluri)))
                                        {
                                            device->resMap.insert (pair<string, t_resource*> (it_key->first,
                                                                                   it_key->second));
                                        }
                                }

                                device->setresourcecallback();
                                it_uid->second->deviceVec.push_back (device);
                                i++;
                        }
                }
                it_uid->second->setTdeviceCall();
        }
}


void t_resource_att_details()
{

int m=1;
	QueryParamsMap test;
	for (auto it_uid = t_thingsmap.cbegin(); it_uid != t_thingsmap.cend();
			++it_uid) {
		for (int i = 0; i < it_uid->second->deviceVec.size(); i++)
		{
				for (auto it_non = it_uid->second->deviceVec[i]->resMap.cbegin();
						it_non != it_uid->second->deviceVec[i]->resMap.cend();
						++it_non)
				{

							t_thingsvector.push_back(it_non->second->key);


							std::cout << "\nFIRST get req sent to : "<< m << "\n"
									<< it_non->second->key
									<< it_non->second->resource->get(test,
									std::bind(&t_resource::onGet, it_non->second,
									_1, _2, _3),OC::QualityOfService::HighQos) << endl;
							usleep(700000);
							m++;

            }
				 }

	/*	for (int i = 0; i < t_thingsvector.size(); i++)
				{
					auto it = it_uid->second->deviceVec[i]->resMap.find(t_thingsvector[i]);
					if (it != it_uid->second->deviceVec[i]->resMap.end()) {
					std::cout << "\n\nSECOND get req sent to : " << it->first <<"\n" <<  it->second->resource->get(test,
																	 std::bind(&t_resource::onGet,  it->second, _1, _2, _3)) << endl;
							usleep(700000);
					}
				}*/
      }
}



void missing()
{
	for (auto it_uid = t_thingsmap.cbegin(); it_uid != t_thingsmap.cend();
			++it_uid)
	{
			if(it_uid->second->osName.empty() )
			{

				for (auto it_key = t_resourceColmap.cbegin(); it_key != t_resourceColmap.cend(); ++it_key)
				{
		            if (!(it_key->first.find (it_uid->first)))
		            {
	cout << "\n\nIn Second function Platform : " << it_key->second->hostadd  << endl << endl;

	std::string platformDiscoveryURI = "/oic/p";
	OCPlatform::getPlatformInfo(it_key->second->hostadd , platformDiscoveryURI, connectivityType,
			&receivedPlatformInfo);

	sleep(3);

						}
					}
			}

	}

}


void SecondDiscovery(string uuid)
{
//	cout << "\n\n\t SENDING SECOND TIME DISCOVERY TO :" << uuid << "\n\n";
	std::ostringstream requestURI;

	auto it = t_thingsmap.find(uuid);
	if (it != t_thingsmap.end()) {

		for (int i = 0; i < it->second->deviceVec.size(); i++)
		{
				for (auto it_non = it->second->deviceVec[i]->resMap.cbegin();
						it_non != it->second->deviceVec[i]->resMap.cend();
						++it_non)
				{
				            for(auto &resourceTypes : it_non->second->resource->getResourceTypes())
				            {
					            std::cout << "\tList of resource types: "<< resourceTypes << std::endl;
				                cout << "\n\nIn function deviceInfo " << endl;
				            	requestURI << OC_RSRVD_WELL_KNOWN_URI << "?rt=" << resourceTypes ;
				            	cout << "Printing Discovery Req :" << requestURI.str() << endl;
				            	OCPlatform::findResource("", requestURI.str(), CT_DEFAULT, &foundResource);
				            	sleep(5);
				            }

				}

	}
}

}


void DeleteThings()
{

cout << "Size of missPlatform : " << missPlatform.size() << endl;
cout << "Size of missDiscovery : " << missDiscovery.size() << endl;
cout << "Size of t_thingsmap1 : " << t_thingsmap.size() << endl;


for(auto it = t_thingsmap.cbegin(); it != t_thingsmap.cend(); ++it)
{
    std::cout <<"BEFORE_t_thingsmapPRINT=" <<it->first << endl;
}


for(auto it = missDiscoveryMap.cbegin(); it != missDiscoveryMap.cend(); ++it)
{
    std::cout <<"BEFORE_missDiscoveryMapPRINT=" <<it->first << endl;
}

for(missPlatformit = missPlatform.begin() ; missPlatformit < missPlatform.end(); missPlatformit++ )
{
    std::cout <<"BEFORE_missPlatformPRINT=" << *missPlatformit << endl;

}

if(missDiscoveryMap.size() < missPlatform.size())
{

cout << "missDiscoveryMap.size():" << missDiscoveryMap.size() << "<"
	 << "missPlatform.size():"     << missPlatform.size() << endl;

			for(auto it = missDiscoveryMap.cbegin(); it != missDiscoveryMap.cend(); ++it)
			{
					missPlatform.erase(std::remove(missPlatform.begin(), missPlatform.end(), it->first), missPlatform.end());

			}

			cout << "After Removing from missPlatform : " << missPlatform.size() << endl;

			for( missPlatformit = missPlatform.begin() ; missPlatformit < missPlatform.end(); missPlatformit++ ) {
			t_thingsmap.erase(*missPlatformit);

			}

}


else if(missDiscoveryMap.size() > missPlatform.size())
{

	cout << "missDiscoveryMap.size():" << missDiscoveryMap.size() << ">"
	<< "missPlatform.size():"     << missPlatform.size() << endl;

			for(missPlatformit = missPlatform.begin() ; missPlatformit < missPlatform.end(); missPlatformit++ )
			{
					missDiscoveryMap.erase(*missPlatformit);
			}

			cout << "After Removing from missDiscoveryMap : " << missDiscoveryMap.size() << endl;

			for(auto it = missDiscoveryMap.cbegin(); it != missDiscoveryMap.cend(); ++it)
			{
						t_thingsmap.erase(it->first);

			}

}

if(t_thingsmap.size() != missPlatform.size()){

	for(auto it = t_thingsmap.begin(); it != t_thingsmap.end(); ++it)
	{
		for(missPlatformit = missPlatform.begin() ; missPlatformit < missPlatform.end(); missPlatformit++ )
		{
		    if( *missPlatformit != it->first ){

		    	SecondDiscovery(it->first);

//		    	for(auto itnon = t_resourcemap.begin(); it != t_resourcemap.end(); ++it)
//		    	{
//			        if (!(itnon->first.find (it->first)))
//			    	t_resourcemap.erase(itnon->first);
//		    	}

		    }
		}
	}
}


for(auto it = t_thingsmap.cbegin(); it != t_thingsmap.cend(); ++it)
{
    std::cout <<"AFTER_t_thingsmapPRINT=" <<it->first << "\t " << it->second->thingsName << endl;
}


}


void end_Discovery(int sig)
{

    missing();

	t_things_garbage();

	DeleteThings();

	reqType = "discovery";
	std::cout << " \n in end_Discovery map: " << '\n';
	std::cout << " \n Non_Collection Map: " << '\n';
	for (auto it = t_resourcemap.cbegin(); it != t_resourcemap.cend(); ++it) {
		std::cout << it->first << "\t:\t" << it->second << "\n";
	}
	std::cout << " \n Collection Map: " << '\n';
	for (auto it = t_resourceColmap.cbegin(); it != t_resourceColmap.cend();
			++it) {
		std::cout << it->first << "\t:\t" << it->second << "\n";
	}

	t_things_garbage();
	t_things_device();
	t_resource_att_details();

	if (auto_Discovery == true)
	{
		signal(SIGALRM, Auto_Discovery);
		alarm(3600);
	}
	else
	{
		signal(SIGALRM, SIG_IGN);
	}
}
