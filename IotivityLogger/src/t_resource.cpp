#include "headers.hpp"

using namespace OC;
using namespace std;

static ObserveType OBSERVE_TYPE_TO_USE = ObserveType::Observe;
extern int getdone;
extern int observedone;

extern string reqType;
extern std::vector<std::string>t_thingsvector;

REDIS hiredisres;
void t_resource::onGet(const HeaderOptions& /*headerOptions*/,
		const OCRepresentation& rep, const int eCode)
{
	try {
		if (eCode == OC_STACK_OK) {

//			setlogging1(OC_LOG_DEBUG,"OC_LOG_DEBUG","Onget response received : ", this->key );

			t_thingsvector.erase(std::remove(t_thingsvector.begin(), t_thingsvector.end(), this->key), t_thingsvector.end());

			this->Attdetals = rep.getPayload()->values;
			getdone++;
			this->callback(rep.getPayload()->values,reqType,this->key,getdone);
//			hiredisres.WriteRedisDevice(this->key,this->Attdetals);

		}

		else {
			  std::ostringstream oss;
			  oss << eCode;
//			setlogging1(OC_LOG_DEBUG,"OC_LOG_DEBUG","OnGET Response error:  : ", oss.str() );
		}
	} catch (std::exception& e) {
		  std::ostringstream oss;
		  oss << e.what();
//		setlogging1(OC_LOG_ERROR,"OC_LOG_ERROR","Exception in OnGET Response : ", oss.str() );

		std::cout << "Exception: " << e.what() << " in onGet" << std::endl;
	}
}

void t_resource::getRepresentation()
{
cout << "In getRepresentation " << endl;
	QueryParamsMap test;
	this->resource->get(test, std::bind(&t_resource::onGet, this, _1, _2, _3));
}

void t_resource :: setnotifycallback (notifycallback callback)
{
        this->callback = callback;
}
void t_resource::onPost(const HeaderOptions& /*headerOptions*/,
		const OCRepresentation& rep, const int eCode) {
	try {

		if (eCode == OC_STACK_OK || eCode == OC_STACK_RESOURCE_CHANGED) {

			cout << "Onpost response received : " << this->key << endl;
			this->Attdetals = rep.getPayload()->values;
			this->callback(rep.getPayload()->values,reqType,this->key,getdone);
//			hiredisres.WriteRedisDevice(this->key,this->Attdetals);
			std::cout << "onPost action done" << std::endl;

		} else {
			std::cout << "onPOST Response error: " << eCode << std::endl;
			std::exit(-1);
		}

	}

	catch (std::exception& e) {
		std::cout << "Exception: " << e.what() << " in onPost" << std::endl;
	}
}

void t_resource::postRepresentation(OCRepresentation rep) {
	std::cout << "posting Representation..." << std::endl;
	this->resource->post(rep, QueryParamsMap(),
			std::bind(&t_resource::onPost, this, _1, _2, _3));
				usleep(500000);

}

void t_resource::onObserve(const HeaderOptions /*headerOptions*/,
		const OCRepresentation& rep, const int& eCode,
		const int& sequenceNumber) {
			cout << "onObserve action Receieve : "<< this->key << endl;
	try {
		if (eCode == OC_STACK_OK && sequenceNumber <= MAX_SEQUENCE_NUMBER)
		{
			if(	this->obsFlag == true)
			{
				std::cout << "Observe registration action is successful Second"<< endl;

			this->callback(rep.getPayload()->values, reqType,this->key, observedone);
			std::cout << "onObserve action done" << this->key << "\t :"<<rep.getPayload()->values->name << std::endl;
			}
			
			if (sequenceNumber == OC_OBSERVE_REGISTER || 	this->obsFlag == false ) {
				this->obsFlag = true;
				std::cout << "Observe registration action is successful First" << endl;

						observedone++;
						this->Attdetals = rep.getPayload()->values;
						this->callback(rep.getPayload()->values, reqType,this->key, observedone);
			}


		} else {
			if (eCode == OC_STACK_OK) {
				std::cout
						<< "No observe option header is returned in the response."
						<< std::endl;
				std::cout
						<< "For a registration request, it means the registration failed"
						<< std::endl;
			} else {
				std::cout << "onObserve Response error: " << eCode << std::endl;
				std::exit(-1);
			}
		}
	} catch (std::exception& e) {
		std::cout << "Exception: " << e.what() << " in onObserve" << std::endl;
	}

}

void t_resource::observeRepresentation() {
	std::cout << "observing Representation..." << std::endl;
	this->resource->observe(OBSERVE_TYPE_TO_USE, QueryParamsMap(),
			std::bind(&t_resource::onObserve, this, _1, _2, _3, _4));
}
