#include "headers.hpp"

using namespace OC;
using namespace std;

#define  discovering   1181
#define  getting        754
#define  posting        772
#define  observeing     1076
#define  print          557

void Auto_Discovery(int);
int getdone = 0;
int observedone = 0;
extern int DisFlag;

extern std::map<string, t_things*> t_thingsmap;   // t_things map
extern bool auto_Discovery;
extern std::vector<std::string> t_thingsvector;


string reqType;
t_device *t_deviceobj1;

void getResource(string uuid, string devid) {
	QueryParamsMap test;
	cout << "In function getResource" << endl;
	getdone = 0;

	auto it_uid = t_thingsmap.find(uuid);
	if (it_uid != t_thingsmap.end()) {
		for (int i = 0; i < it_uid->second->deviceVec.size(); i++) {
			if (devid.compare(it_uid->second->deviceVec[i]->Coluri) == 0) {
				for (auto it_key =
						it_uid->second->deviceVec[i]->resMap.cbegin();
						it_key != it_uid->second->deviceVec[i]->resMap.cend();
						++it_key) {
					if (it_key != it_uid->second->deviceVec[i]->resMap.end()) {

						t_thingsvector.push_back(it_key->second->key);

						if (it_key->second->key
								== "5b000000-ed00-4000-6301-000011010000/SMWS1M_01/onboard") {

						} else {
							cout << "SIZE OF t_thingsvector :"
									<< t_thingsvector.size() << endl;

							std::cout << "get req sent to:"
									<< it_key->second->key << endl;
							it_key->second->getRepresentation();
							sleep(2);
						}
					}
				}
			}
		}
	}
}

void postResource(string uuid, string devid, string key, string attributeName,
		string inputValue) {
	attributetype attributeValue;
	OCRepresentation rep;
	struct property *temp;

	std::map<string, t_resource*>::iterator resItr;

	auto it_uid = t_thingsmap.find(uuid);
	if (it_uid != t_thingsmap.end()) {
		for (int i = 0; i < it_uid->second->deviceVec.size(); i++) {
			if (devid.compare(it_uid->second->deviceVec[i]->Coluri) == 0) {

				auto it = it_uid->second->deviceVec[i]->resMap.find(key);
				if (it != it_uid->second->deviceVec[i]->resMap.end()) {
					cout << "In resMap with uri: " << it->first << endl;
					temp = it_uid->second->deviceVec[i]->pro_data.head;

					while (temp) {
						string name = string(temp->name);
						if (name.compare(attributeName) == 0) {
							switch (temp->type) {

							case 0:
								cout << "OCREP_PROP_NULL" << endl;
								break;
							case 1:
								cout << "OCREP_PROP_INT_postResource" << endl;
								attributeValue.i = stoi(inputValue);
								rep.setValue(attributeName, attributeValue.i);
								break;
							case 2:
								cout << "OCREP_PROP_DOUBLE_postResource"
										<< endl;
								attributeValue.d = stod(inputValue);
								rep.setValue(attributeName, attributeValue.d);
								break;
							case 3:
								cout << "OCREP_PROP_BOOL_postResource" << endl;
								if (inputValue == "true")
									attributeValue.b = 1;
								else if (inputValue == "false")
									attributeValue.b = 0;
								rep.setValue(attributeName, attributeValue.b);
								break;
							case 4:
								cout << "OCREP_PROP_STRING_postResource"
										<< endl;
								attributeValue.str = inputValue;
								rep.setValue(attributeName, attributeValue.str);
								break;

							case 7: {
								cout << "OCREP_PROP_ARRAY_postResource" << endl;

								cout << "OCREP_PROP_ARRAY1" << endl;

								std::vector<int> chroma;

								cout << "AtrValue: " << inputValue << endl;
								std::replace(inputValue.begin(),
										inputValue.end(), ',', ' ');
								std::stringstream iss(inputValue);
								int number;
								while (iss >> number) {
									chroma.push_back(number);
								}
								rep.setValue(attributeName, chroma);

								for (int s : chroma) {
									std::cout << s << endl;
								}
								chroma.clear();

							}
								break;

							}

						}
						temp = temp->next;
					}
					it->second->postRepresentation(rep);
				}
			}
		}
	}
}

void observeResource(string uuid, string devid) {
	cout << "In function observeResource" << endl;
	QueryParamsMap test;
	observedone = 0;
	auto it_uid = t_thingsmap.find(uuid);
	if (it_uid != t_thingsmap.end()) {
		for (int i = 0; i < it_uid->second->deviceVec.size(); i++) {
			if (devid.compare(it_uid->second->deviceVec[i]->Coluri) == 0) {
				for (auto it_key =
						it_uid->second->deviceVec[i]->resMap.cbegin();
						it_key != it_uid->second->deviceVec[i]->resMap.cend();
						++it_key) {
					if (it_key != it_uid->second->deviceVec[i]->resMap.end()) {

						t_thingsvector.push_back(it_key->second->key);

						if (it_key->second->key
								== "5b000000-ed00-4000-6301-000011010000/SMWS1M_01/onboard") {
						} else {
							//cout << "SIZE OF t_thingsvector :" << t_thingsvector.size()<< endl;

							std::cout << "observe req sent to:"
									<< it_key->second->key << endl;
							it_key->second->observeRepresentation();
							sleep(2);
						}
					}
				}
			}
		}
	}
}

int convertToASCII(string letter) {

	int m = 0;
	for (int i = 0; i < letter.length(); i++) {
		char x = letter.at(i);
		m += int(x);
	}
	return m;
}

void one_time_Discovery() {
	cout << "In one_time_Discovery" << endl;
	signal(SIGALRM, end_Discovery);
	Discovery_all();
}

void Auto_Discovery(int sig) {
	printf("IN Auto_Discovery\n");
	one_time_Discovery();
//      alarm(3600);
}

void DisRequest(int type) {
	printf("IN DisRequest\n");

	switch (type) {
	case 1:
		std::cout << "in one_time_Discovery" << '\n';
		one_time_Discovery();
		break;

	case 2:
		signal(SIGALRM, Auto_Discovery);
		auto_Discovery = true;
		std::cout << "in Auto_Discovery" << '\n';
		alarm(1);
		break;

	case 3:
		signal(SIGALRM, SIG_IGN);
		auto_Discovery = false;
		std::cout << "in Disable_Auto_Discovery" << '\n';
		break;

	default:
		std::cout << "in one_time_Discovery" << '\n';
		one_time_Discovery();
		break;
	}

	printf("OUT myThread_Dis\n");

}

void ResControl(struct GrpcRequest control) {

	int option = convertToASCII(control.command);

	string attributeName, attributeValue;
	string key, uuid, devid;

	uuid = control.UUID;
	devid = control.UUID + control.DevID;
	key = devid + "/" + control.map;

	cout << "REQUEST :" << control.command << endl;
	cout << "UUID :" << uuid << endl;
	cout << "DEVICEID :" << devid << endl;
	cout << "KEY :" << key << endl;

	attributeName = control.attributeName1;
	attributeValue = control.attributeValue1;

	cout << "ATR_NAME :" << attributeName << endl;
	cout << "ATR_VAL :" << attributeValue << endl;
	switch (option) {
	case getting: {
		cout << "::::::::TIK-Client-Get-Request::::::::" << endl;
		reqType = "get";

		getResource(uuid, devid);
	}
		break;

	case posting: {
		cout << "::::::::TIK-Client-Post-Request::::::::" << endl;
		reqType = "post";
		postResource(uuid, devid, key, attributeName, attributeValue);
	}
		break;

	case observeing: {
		reqType = "observe";
		cout << "::::::::TIK-Client-Observe-Request::::::::" << endl;
		observeResource(uuid, devid);
	}
		break;
	}
}

void sendrequest(struct GrpcRequest reqStruct) {

	int init = 1;
	int request = convertToASCII(reqStruct.command);

	cout << "request: " << request << endl;

	switch (request) {
	case discovering: {

		cout << "::::::::TIK-Client-Discovery-Request::::::::" << endl;
		DisFlag = 0;
		thread([init]() {DisRequest(init);}).detach();
	}
		break;

	default:
		cout << "Request for control" << endl;
		if (reqType == "discovery") {
			std::cout << "wait untill discovery complete" << '\n';
		} else {
			std::cout << "control happening ...." << '\n';
			thread([reqStruct]() {ResControl(reqStruct);}).detach();
		}
		break;
	}
}

int main() {


	time_t t = time(NULL);
 	struct tm *tm = localtime(&t);
	char s[64];
    	strftime(s, sizeof(s), "%c", tm);

    oc_log_level level;
	#ifdef DEBUG
	 level=OC_LOG_DEBUG;
	#elif ERROR 
	 level=OC_LOG_ERROR;
	#elif INFO
	 level=OC_LOG_INFO;
	#elif FATAL
	 level=OC_LOG_FATAL;
	#elif DISABLED
	 level=OC_LOG_DISABLED;
	#elif ALL
	 level=OC_LOG_ALL;
	#endif
	 setlevel1(level);
	printf("you are builded with level=%d\n",level);

	setlogging1(OC_LOG_DISABLED,"OC_LOG_DISABLED","Hello, World!");
	setlogging1(OC_LOG_ALL,"OC_LOG_ALL","Hello, World!");
	setlogging1(OC_LOG_DEBUG,"OC_LOG_DEBUG","Hello, World!");
	setlogging1(OC_LOG_ERROR,"OC_LOG_ERROR","Hello, World!");
	setlogging1(OC_LOG_FATAL,"OC_LOG_FATAL","Hello, World!");
	setlogging1(OC_LOG_INFO,"OC_LOG_INFO","Hello, World!");

	std::cout.setf(std::ios::boolalpha);
	int init = 1;

	thread([init]() {DisRequest(init);}).detach();

	std::cout << "\nStarting the GRPC Server " << endl;

	runserver(GRPCCppServer);   //sddress of GRPC server

	return 0;
}
