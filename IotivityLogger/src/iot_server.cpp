#include "headers.hpp"

std::map<string, t_things*> t_thingsmapobj;
t_device *t_deviceobj;

void SettingDevProp(t_device *t_deviceObjRes, device *dev, string devID) {


	struct property *temp = t_deviceObjRes->pro_data.head;

	while (temp) {

		tproperty *prop = dev->add_properties();

		prop->set_property_map(temp->map);
		prop->set_property_name(temp->name);

		stringstream stringAtrVal;
		switch (temp->type) {

		case 0:
			cout << "OCREP_PROP_NULL " << endl;
			break;
		case 1: {
			cout << "OCREP_PROP_INT " << endl;
			stringAtrVal << temp->i;
			string integerval = stringAtrVal.str();
			prop->set_property_type("int");
			prop->set_property_value(integerval);

		}
			break;
		case 2: {
			cout << "OCREP_PROP_DOUBLE  " << endl;
			stringAtrVal << temp->d;
			string doubleval = stringAtrVal.str();
			prop->set_property_type("double");
			prop->set_property_value(doubleval);

		}
			break;
		case 3: {
			cout << "OCREP_PROP_BOOL  " << endl;
			prop->set_property_type("boolean");
			cout << "BOOl :  " << temp->b  << endl;

			if (temp->b == true)
				prop->set_property_value("1");
			else
				prop->set_property_value("0");
		}
			break;
		case 4: {
			cout << "OCREP_PROP_STRING  " << endl;
			prop->set_property_type("string");
			prop->set_property_value(temp->str);
		}
			break;
		case 5:
			cout << "OCREP_PROP_BYTE_STRING" << endl;
			break;
		case 6:
			cout << "OCREP_PROP_OBJECT" << endl;
			break;
		case 7:
			{
			cout << "OCREP_PROP_ARRAY" << endl;
		  	std::ostringstream oss;

			cout << "OCREP_PROP_ARRAY_grpc" << endl;
			cout << "OCREP_PROP_STRING  " << endl;
			prop->set_property_type("string");
			oss <<"[";
			for (int i=0;i<temp->intArray.size();i++)
			{
			        oss << temp->intArray[i]<<",";
			}
                        oss <<"]";
                        string stp=oss.str();
                        stp.erase (stp.size()-2, 1);
                        cout << stp<<"\n";
			prop->set_property_value(stp);
			}
			break;
		}
		temp = temp->next;
	}
}

void client::NondiscoveryResponse(t_device* t_deviceObjRes) {
	cout << "Printing the NonDiscoveryResponse GRPC" << endl;

	NondiscoveryObject go;
	device *dev = go.mutable_dev();

	string sentence = t_deviceObjRes->Coluri;
	sentence = sentence.substr(sentence.find_first_of("/") + 1);
	dev->set_did(sentence);

	istringstream iss(t_deviceObjRes->Coluri);
	string s;
	int counter = 0;
	while (getline(iss, s, '/')) {
		if (counter == 0)
			go.set_uuid(s.c_str());
		else
			dev->set_did(s.c_str());
		counter++;
	}

	SettingDevProp(t_deviceObjRes, dev, t_deviceObjRes->Coluri);

	status reply;
	ClientContext context;
	cout << "sending NonDiscovery Response to node js client ................."
			<< endl;
	stub_->NondiscoveryResponse(&context, go, &reply);


}

void client::DiscoveryResponse(std::map<string, t_things*> t_thingsmapobj) {

	cout << "Printing the DiscoveryResponse GRPC" << endl;
	ResponseObject res;
	for (auto ThingsItr = t_thingsmapobj.cbegin();
			ThingsItr != t_thingsmapobj.cend(); ++ThingsItr) {

		tthing *thing = res.add_tthings();

		thing->set_thingsname(ThingsItr->second->thingsName);
		thing->set_osname(ThingsItr->second->osName);
		thing->set_thingstype(ThingsItr->second->thingsType);
		thing->set_hardwareversion(ThingsItr->second->hardwareVersion);
		thing->set_manufacturername(ThingsItr->second->manufacturerName);
		thing->set_uuid(ThingsItr->second->uuid);

		for (int i = 0; i < ThingsItr->second->deviceVec.size(); i++) {

			device *dev = thing->add_devices();
			string sentence = ThingsItr->second->deviceVec[i]->Coluri;
			sentence = sentence.substr(sentence.find_first_of("/") + 1);
			dev->set_did(sentence);

			SettingDevProp(ThingsItr->second->deviceVec[i], dev,
					ThingsItr->second->deviceVec[i]->Coluri);

		}
	}

	status reply;
	ClientContext context;
	cout << "sending Discovery Response to node js client  ................."
			<< endl;
	stub_->DiscoveryResponse(&context, res, &reply);
}

void request_method(const RequestObject* nodejsreq) {
	struct GrpcRequest iotStruct;
	iotStruct.command = nodejsreq->command();
	iotStruct.UUID = nodejsreq->uuid();
	iotStruct.DevID = nodejsreq->devid();
	iotStruct.map = nodejsreq->map();
	iotStruct.attributeName1 = nodejsreq->attribute_name();
	iotStruct.attributeValue1 = nodejsreq->attribute_value();

	sendrequest(iotStruct);
}

class serviceImpl final : public Sender::Service {
	Status Request(ServerContext* context, const RequestObject* nodejsreq,
			status* stats) override
			{
		if (nodejsreq->command() == "getting" || nodejsreq->command() == "posting" || nodejsreq->command() == "observeing"
				|| nodejsreq->command() == "discovering") {
			stats->set_message(true);
			request_method(nodejsreq);
		} else
			stats->set_message(false);
		return Status::OK;
	}
};

void runserver(string server_add) {
	serviceImpl service;
	ServerBuilder builder;
	builder.AddListeningPort(server_add, grpc::InsecureServerCredentials());
	builder.RegisterService(&service);
	unique_ptr<Server> server(builder.BuildAndStart());
	cout << "Server listening on " << server_add << endl;
	server->Wait();
}
