/*
 * headers.h
 *
 *  Created on: 15-Jun-2017
 *      Author: smartron
 */

#ifndef HEADERS_H_
#define HEADERS_H_

#include "iotivity_config.h"
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_PTHREAD_H
#include <pthread.h>
#endif
#ifdef HAVE_WINDOWS_H
#include <Windows.h>
#endif
#include <string>
#include <map>
#include <cstdlib>
#include <mutex>
#include <condition_variable>
#include "OCPlatform.h"
#include "OCApi.h"
#include <signal.h>
#include <ctime>
#include <stdarg.h>
#include <cstdarg>
#include <time.h>

#include "samplegrpc.hpp"
#include "t_property.hpp"
#include "t_resource.hpp"
#include "t_devices.hpp"
#include "t_things.hpp"
#include "iot_server.hpp"
#include "t_discovery.hpp"
#include "hirediswrite.hpp"

#include "oc_ostream_logger.h"
#include "oc_log_stream.hpp"

using std::placeholders::_1;
using std::placeholders::_2;
using std::placeholders::_3;
using std::placeholders::_4;

#define GRPCCppServer		 "localhost:2222"   //		iotivity will receive the request to this address from node js client
#define GRPCNodeJsClient    	 "localhost:1111"   //		iotivity will send the response to this address of node js client

#endif /* HEADERS_H_ */

