/*
 * hirediswrite.h
 *
 *  Created on: 13-Jun-2017
 *      Author: jotirling
 */

#ifndef HIREDISWRITE_H_
#define HIREDISWRITE_H_

#include <iostream>
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "hiredis.h"
using namespace std;

class REDIS
{
public:
void Context(void);
void WriteRedisDevice(string hash_key, OCRepPayloadValue* repp);
void WriteRedisThings(std::string hash_key, std::string discinfo);
};


#endif /* HIREDISWRITE_H_ */
